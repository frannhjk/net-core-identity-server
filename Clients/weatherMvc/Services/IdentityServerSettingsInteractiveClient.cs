namespace weatherMvc.Services
{
    public class IdentityServerSettingsInteractiveClient
    {
        public string AuthorityUrl {get; set;}
        public string ClientId {get; set;}
        public string ClientSecret { get; set;}
    }
}