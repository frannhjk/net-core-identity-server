namespace weatherMvc.Services
{
    /// <summary>
    /// Configured in AppSettings.json file
    /// </summary>
    public class IdentityServerSettings
    {
        public string DiscoveryUrl { get; set; }
        public string ClientName { get; set; }
        public string ClientPassword { get; set; } // Client Secret 
        public bool UseHttps { get; set; }
    }
}