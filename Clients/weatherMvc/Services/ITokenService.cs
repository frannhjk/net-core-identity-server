using System.Threading.Tasks;
using IdentityModel.Client;

namespace weatherMvc.Services
{
    public interface ITokenService
    {
        Task<TokenResponse> GetTokenForClientCrendentials(string scope);
        //Task<TokenRevocationResponse> RevokeToken(string accessToken);
    }
}