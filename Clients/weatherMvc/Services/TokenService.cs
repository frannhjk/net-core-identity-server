using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace weatherMvc.Services
{
    public class TokenService : ITokenService
    {
        private readonly ILogger<TokenService> _logger;                                
        private readonly IOptions<IdentityServerSettings> _identityServerSettings;     
        private readonly IOptions<IdentityServerSettingsInteractiveClient> _interactiveIDPSettings;
        private readonly DiscoveryDocumentResponse _discoveryDocument;     
        
        public TokenService(
            ILogger<TokenService> logger, 
            IOptions<IdentityServerSettings> identityServerSettings,
            IOptions<IdentityServerSettingsInteractiveClient> interactiveIDPSettings)
        {
            _logger = logger;
            _identityServerSettings = identityServerSettings;
            _interactiveIDPSettings = interactiveIDPSettings;
            
            using var client = new HttpClient();
            _discoveryDocument = client.GetDiscoveryDocumentAsync(identityServerSettings.Value.DiscoveryUrl).Result;

            if (_discoveryDocument.IsError)
            {
                _logger.LogError($"Unable to get Discovery document, ERROR: {_discoveryDocument.Error}");
                throw new Exception($"Unable to get Discovery document, ERROR: {_discoveryDocument.Error}");
            }
        }

        // Requesting Token using client_Credentials
        public async Task<TokenResponse> GetTokenForClientCrendentials(string scope)
        {
            using var client = new HttpClient();   
            
            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                // TODO: Cambiar a los valores de Client_Credentials.
                Address = _discoveryDocument.TokenEndpoint,
                ClientId = _identityServerSettings.Value.ClientName,
                ClientSecret = _identityServerSettings.Value.ClientPassword,
                Scope = scope
            });

            if (tokenResponse.IsError)
            {
                _logger.LogError($"Unable to get the Token, ERROR: {tokenResponse.Error}");   
                throw new Exception($"Unable to get the Token, ERROR: {tokenResponse.Error}");
            }               

            return tokenResponse;
        }


        // NO FUNCIONA AUN
        /*
        public async Task<TokenRevocationResponse> RevokeToken(string accessToken)
        {
            using var client = new HttpClient();
            

            var tokenRevoecationResponse = await client.RevokeTokenAsync(new TokenRevocationRequest
            {
                Address = _discoveryDocument.RevocationEndpoint,
                ClientId =  _interactiveIDPSettings.Value.ClientId,
                ClientSecret = _interactiveIDPSettings.Value.ClientSecret,
                
                Token = accessToken
            });
            
            if (tokenRevoecationResponse.IsError)
            {
                _logger.LogError($"Unable to get the Token, ERROR: {tokenRevoecationResponse.Error}");   
                throw new Exception($"Unable to get the Token, ERROR: {tokenRevoecationResponse.Error}");
            }

            return tokenRevoecationResponse;
        }*/
    }
}
