﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using weatherMvc.Models;
using weatherMvc.Services;

namespace weatherMvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ITokenService _tokenService;

        public HomeController(ILogger<HomeController> logger, ITokenService tokenService)
        {
            _logger = logger;
            _tokenService = tokenService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        [Authorize]
        public async Task<IActionResult> Weather()
        {
            var data = new List<WeatherData>();

            using (var client = new HttpClient())
            {
                // Obtengo el token del IDP (IDP/connect/token)
                var tokenResponse = await _tokenService.GetTokenForClientCrendentials("weatherapi.read");
                
                // Lo agrego al Header del request
                client
                    .SetBearerToken(tokenResponse.AccessToken);
                
                // Test values
                var testRefreshValue = tokenResponse.RefreshToken;
                var accessToken = tokenResponse.AccessToken;
                
                _logger.LogInformation($"ACCESSTOKEN: {tokenResponse.AccessToken}");
                _logger.LogInformation($"REFRESHTOKEN: {tokenResponse.RefreshToken}");

                var result = await client
                    .GetAsync("https://localhost:5002/weatherforecast")
                    .ConfigureAwait(false);

                if (result.IsSuccessStatusCode)
                {
                    var model = await result.Content.ReadAsStringAsync();

                    data = JsonConvert.DeserializeObject<List<WeatherData>>(model);

                    return View(data);
                }
                else
                {
                    throw  new Exception("Unable to get content");
                }
            }
        }

        /*
        public async Task<IActionResult> Revoke()
        {
            
            using (var client = new HttpClient())
            {
                var tokenResponse = await _tokenService.GetToken("weatherapi.read");

                _logger.LogInformation($"Access from revoke: {tokenResponse.AccessToken}");

                var test = tokenResponse.AccessToken;
                
                var revokeResponse = await _tokenService.RevokeToken(test);

                _logger.LogInformation($"RESPONSE FROM REVOKE: {revokeResponse.Json}");

                return View("Index");
            }
        }*/
    }
}