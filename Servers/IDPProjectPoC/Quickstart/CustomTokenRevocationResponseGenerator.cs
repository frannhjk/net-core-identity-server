﻿using System;
using System.Threading.Tasks;
using IdentityServer4.ResponseHandling;
using IdentityServer4.Stores;
using IdentityServer4.Validation;
using Microsoft.Extensions.Logging;

namespace IDPProjectPoC.Quickstart
{
    public class CustomTokenRevocationResponseGenerator : TokenRevocationResponseGenerator, ITokenRevocationResponseGenerator
    {
        /// <summary>
        /// Revoke refresh token only if it belongs to client doing the request
        /// </summary>
        protected override async Task<bool> RevokeRefreshTokenAsync(TokenRevocationRequestValidationResult validationResult)
        {
            Logger.LogInformation("Revoking refresh token");
            //assume the token is access token instead of refresh token
            var token = await ReferenceTokenStore.GetReferenceTokenAsync(validationResult.Token);
            if (token != null)
            {
                Logger.LogInformation($"Revoking refresh token for clientId {token.ClientId} ");
                if (token.ClientId == validationResult.Client.ClientId)
                {
                    Logger.LogDebug("Refresh token revoked");
                    await RefreshTokenStore.RemoveRefreshTokensAsync(token.SubjectId, token.ClientId);
                    await ReferenceTokenStore.RemoveReferenceTokensAsync(token.SubjectId, token.ClientId);
                }
                else
                {
                    Logger.LogWarning("Client {clientId} tried to revoke a refresh token belonging to a different client: {clientId}", validationResult.Client.ClientId, token.ClientId);
                }

                return true;
            }

            return false;
        }



        public CustomTokenRevocationResponseGenerator(IReferenceTokenStore referenceTokenStore, IRefreshTokenStore refreshTokenStore, ILogger<TokenRevocationResponseGenerator> logger) : base(referenceTokenStore, refreshTokenStore, logger)
        {
        }
    }

}

